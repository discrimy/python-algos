from typing import MutableSequence, TypeVar

T = TypeVar("T")


def bubble_sort(seq: MutableSequence[T]):
    """
    Сортирует последовательность :param arr пузырком.
    Порядок сортировки: по возрастанию

    Временная сложность: O(n^2)

    Сложность по памяти: О(n)

    :param seq: последовательность, которую нужно отсортировать. Сортировка происходит на месте
    """
    is_sorted = False
    while not is_sorted:
        is_sorted = True
        # Проходимся по всем элементам по парам ( (0, 1), (1, 2), (2, 3), .., (n-1, n) )
        for i in range(1, len(seq)):
            # Если элемент позади больше элемента спереди, то пропихиваем бОльший вправо
            if seq[i - 1] > seq[i]:
                seq[i - 1], seq[i] = seq[i], seq[i - 1]
                is_sorted = False
