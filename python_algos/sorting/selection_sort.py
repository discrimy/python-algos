import operator
from typing import MutableSequence, TypeVar

T = TypeVar("T")


def selection_sort(seq: MutableSequence[T]):
    """
    Сортирует последовательность :param seq выбором.
    Порядок сортировки: по возрастанию

    Временная сложность: O(n^2)

    Сложность по памяти: О(n)

    :param seq: последовательность, которую нужно отсортировать. Сортировка происходит на месте
    """
    for i in range(len(seq)):
        # Итерируется по списку (index, val) и вычисляет минимум через 2 аргумент
        # ( (index, val)[1] )
        # enumerate(seq[i:], i) итерируется только по ещё не сортированной части seq
        min_unordered_i, _ = min(enumerate(seq[i:], i), key=operator.itemgetter(1))
        seq[i], seq[min_unordered_i] = seq[min_unordered_i], seq[i]
