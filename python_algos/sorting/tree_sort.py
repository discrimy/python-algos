from typing import MutableSequence, TypeVar

from python_algos.structures.bst import BinarySearchTree

T = TypeVar("T")


def tree_sort(seq: MutableSequence[T]):
    """
    Сортирует список :param seq через построение ДДП.
    Порядок сортировки: по возрастанию

    Временная сложность: O(nlogn)

    Сложность по памяти: О(n)

    :param seq: список, которую нужно отсортировать. Сортировка происходит на месте
    """
    tree = BinarySearchTree(initial_values=seq)
    for i, elem in enumerate(node.value for node in tree.traverse_inorder()):
        seq[i] = elem
