def factorial(n: int) -> int:
    if n < 0:
        raise ValueError("N must be not negative")
    if n == 0:
        return 1
    return n * factorial(n - 1)


Probability = float


def combinations_count(
    all_count: int, needed_count: int, repetitions: bool = False
) -> int:
    if repetitions:
        return combinations_count(all_count + needed_count - 1, needed_count)
    return factorial(all_count) // (
        factorial(needed_count) * factorial(all_count - needed_count)
    )


def permutations_count(
    all_count: int, needed_count: int, repetitions: bool = False
) -> int:
    if repetitions:
        return all_count ** needed_count
    return factorial(all_count) // factorial(all_count - needed_count)


def bernoulli_formula(all_count: int, needed_count: int, p: Probability) -> Probability:
    return (
        C(all_count, needed_count)
        * p ** needed_count
        * (1 - p) ** (all_count - needed_count)
    )


def bernoulli_formula_range(
    all_count: int, from_needed: int, to_needed: int, p: Probability
) -> Probability:
    return sum(
        bernoulli_formula(all_count, needed_count, p)
        for needed_count in range(from_needed, to_needed + 1)
    )


# Aliases
C = combinations_count
A = permutations_count
