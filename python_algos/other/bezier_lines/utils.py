from __future__ import annotations

import itertools
import math
from typing import NamedTuple, Generator, TypeVar, Iterable, Tuple


class Point(NamedTuple):
    x: float
    y: float

    def __mul__(self, other: float) -> Point:
        return Point(self.x * other, self.y * other)

    def __truediv__(self, other: float) -> Point:
        return self * (1 / other)

    def __add__(self, other: Point) -> Point:
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other: Point) -> Point:
        return self + (-other)

    def __neg__(self) -> Point:
        return Point(-self.x, -self.y)

    def length(self) -> float:
        return math.sqrt(self.x ** 2 + self.y ** 2)


def float_range(start: float, end: float, delta: float) -> Generator[float, None, None]:
    while start < end:
        yield start
        start += delta


T = TypeVar("T")


def pairwise(iterable: Iterable[T]) -> Iterable[Tuple[T, T]]:
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)
