from matplotlib import pyplot as plt

from python_algos.other.bezier_lines import Point, find_bezier_point, float_range


def draw_bezier_line(point1: Point, point2: Point, *other: Point) -> None:
    original_points = [point1, point2, *other]
    points = [find_bezier_point(t, *original_points) for t in float_range(0, 1, 0.01)]
    plt.plot([point.x for point in points], [point.y for point in points])
    plt.plot(
        [point.x for point in original_points],
        [point.y for point in original_points],
        linestyle="dotted",
        marker="o",
    )


if __name__ == "__main__":
    draw_bezier_line(Point(0, 0), Point(0, 4), Point(3, 4), Point(6, 1))
    plt.show()
