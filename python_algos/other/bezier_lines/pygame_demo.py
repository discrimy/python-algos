import textwrap
from typing import List, Optional

import pygame

from python_algos.other.bezier_lines import find_bezier_point
from python_algos.other.bezier_lines.utils import Point, float_range


def get_near_point_index(points: List[Point], point: Point) -> Optional[int]:
    for i, p in enumerate(points):
        if (p - point).length() < 20:
            return i


WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREY = (128, 128, 128)
RED = (255, 0, 0)
size = (400, 300)
if __name__ == "__main__":
    print(
        textwrap.dedent(
            """
    Short instruction:
    - Right mouse button - drag and move point
    - Left mouse button - create a new point (if under empty space) or destroy selected point
    """
        )
    )

    pygame.init()
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption("Bezier line example")
    done = False
    clock = pygame.time.Clock()
    pivots = [Point(0, 0), Point(10, 20), Point(300, 50), Point(100, 200)]
    dragging_pivot = None

    while not done:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    dragging_pivot = None
            if event.type == pygame.MOUSEMOTION:
                if dragging_pivot is not None:
                    pivots[dragging_pivot] = Point(*event.pos)
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_point = Point(*event.pos)
                if event.button == pygame.BUTTON_LEFT:
                    dragging_pivot = get_near_point_index(pivots, mouse_point)
                if event.button == pygame.BUTTON_RIGHT:
                    near_point = get_near_point_index(pivots, mouse_point)
                    if near_point is not None and dragging_pivot is None:
                        if len(pivots) > 3:
                            del pivots[near_point]
                    else:
                        pivots.append(mouse_point)

        screen.fill(WHITE)
        points = [find_bezier_point(t, *pivots) for t in float_range(0, 1, 0.01)]
        pygame.draw.aalines(
            screen,
            BLACK,
            False,
            points,
        )
        pygame.draw.aalines(screen, GREY + (128,), False, pivots)
        for i, pivot in enumerate(pivots):
            pygame.draw.circle(screen, GREY if dragging_pivot != i else RED, pivot, 4)

        pygame.display.flip()
