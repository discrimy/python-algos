from __future__ import annotations

"""
Bezier lines realization.

Calculation of line is based on recursive de Cateljau's algorithm
(see https://en.wikipedia.org/wiki/De_Casteljau%27s_algorithm)

There are can by some performance improvements based on dynamic
next point calculation period (straight part requires less frequency of calculation,
others requires more).
This can be done via second derivative - than abs is then less period should be.
"""

from typing import List

from python_algos.other.bezier_lines.utils import Point, float_range, pairwise


def find_bezier_point(t: float, point1: Point, point2: Point, *other: Point) -> Point:
    """
    Recursive formula of Bezier line point calculation
    :param t: from 0 (begging) to 1 (end)
    :param point1: begging of line
    :param point2: second pivot point, can be the last
    :param other: other pivots, can be empty
    """
    if not other:
        return point1 * (1 - t) + point2 * t
    else:
        points = [
            p1 * (1 - t) + p2 * t for p1, p2 in pairwise([point1, point2, *other])
        ]
        return find_bezier_point(t, *points)


def linear_line(point1: Point, point2: Point) -> List[Point]:
    """Function for linear line (just straight line)"""
    return [find_bezier_point(t, point1, point2) for t in float_range(0, 1, 0.01)]


def square_line(point1: Point, point2: Point, point3: Point) -> List[Point]:
    """Function for square line (parabola like)"""
    return [
        find_bezier_point(t, point1, point2, point3) for t in float_range(0, 1, 0.01)
    ]


def cubic_line(
    point1: Point, point2: Point, point3: Point, point4: Point
) -> List[Point]:
    """Function for cubic line (cubic parabola like)"""
    return [
        find_bezier_point(t, point1, point2, point3, point4)
        for t in float_range(0, 1, 0.01)
    ]
