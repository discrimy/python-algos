import itertools
from typing import Iterable, Tuple, TypeVar

BYTE_TO_B64 = {
    0: "A",
    1: "B",
    2: "C",
    3: "D",
    4: "E",
    5: "F",
    6: "G",
    7: "H",
    8: "I",
    9: "J",
    10: "K",
    11: "L",
    12: "M",
    13: "N",
    14: "O",
    15: "P",
    16: "Q",
    17: "R",
    18: "S",
    19: "T",
    20: "U",
    21: "V",
    22: "W",
    23: "X",
    24: "Y",
    25: "Z",
    26: "a",
    27: "b",
    28: "c",
    29: "d",
    30: "e",
    31: "f",
    32: "g",
    33: "h",
    34: "i",
    35: "j",
    36: "k",
    37: "l",
    38: "m",
    39: "n",
    40: "o",
    41: "p",
    42: "q",
    43: "r",
    44: "s",
    45: "t",
    46: "u",
    47: "v",
    48: "w",
    49: "x",
    50: "y",
    51: "z",
    52: "0",
    53: "1",
    54: "2",
    55: "3",
    56: "4",
    57: "5",
    58: "6",
    59: "7",
    60: "8",
    61: "9",
    62: "+",
    63: "/",
}
B64_TO_BYTE = {
    "A": 0,
    "B": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8,
    "J": 9,
    "K": 10,
    "L": 11,
    "M": 12,
    "N": 13,
    "O": 14,
    "P": 15,
    "Q": 16,
    "R": 17,
    "S": 18,
    "T": 19,
    "U": 20,
    "V": 21,
    "W": 22,
    "X": 23,
    "Y": 24,
    "Z": 25,
    "a": 26,
    "b": 27,
    "c": 28,
    "d": 29,
    "e": 30,
    "f": 31,
    "g": 32,
    "h": 33,
    "i": 34,
    "j": 35,
    "k": 36,
    "l": 37,
    "m": 38,
    "n": 39,
    "o": 40,
    "p": 41,
    "q": 42,
    "r": 43,
    "s": 44,
    "t": 45,
    "u": 46,
    "v": 47,
    "w": 48,
    "x": 49,
    "y": 50,
    "z": 51,
    "0": 52,
    "1": 53,
    "2": 54,
    "3": 55,
    "4": 56,
    "5": 57,
    "6": 58,
    "7": 59,
    "8": 60,
    "9": 61,
    "+": 62,
    "/": 63,
}


T = TypeVar("T")


def chunk(source: Iterable[T], size: int, fill: T) -> Iterable[Tuple[T, ...]]:
    return itertools.zip_longest(*([iter(source)] * size), fillvalue=fill)


def base64_encode(source: bytes) -> str:
    if len(source) % 3 == 0:
        padding = 0
    else:
        padding = 3 - (len(source) % 3)
    source += bytes([0b0] * padding)

    result = ""
    for b1, b2, b3 in chunk(source, 3, 0):
        # 00111111 00112222 00222233 00333333
        g1 = (b1 & 0b11111100) >> 2
        g2 = ((b1 & 0b00000011) << 4) + ((b2 & 0b11110000) >> 4)
        g3 = ((b2 & 0b00001111) << 2) + ((b3 & 0b11000000) >> 6)
        g4 = b3 & 0b00111111
        for g in (g1, g2, g3, g4):
            result += BYTE_TO_B64[g]
    if padding:
        return result[:-padding] + "=" * padding
    return result


def _b64_to_byte(b64: str) -> int:
    try:
        return B64_TO_BYTE[b64]
    except KeyError:
        raise ValueError(f"Unknown Base64 symbol: {b64}")


def base64_decode(source: str) -> bytes:
    if len(source) % 4 != 0:
        raise ValueError("Base64 must be 4n length")

    if source[-2:] == "==":
        padding = 2
    elif source[-1:] == "=":
        padding = 1
    else:
        padding = 0

    result = bytearray()
    source_iter = (_b64_to_byte(s) for s in source.replace("=", "", 2))
    for g1, g2, g3, g4 in chunk(source_iter, 4, 0):
        # 00111111 00112222 00222233 00333333
        b1 = ((g1 & 0b00111111) << 2) + ((g2 & 0b00110000) >> 4)
        b2 = ((g2 & 0b00001111) << 4) + ((g3 & 0b00111100) >> 2)
        b3 = ((g3 & 0b00000011) << 6) + g4
        for b in (b1, b2, b3):
            result.append(b)
    if padding:
        return bytes(result[:-padding])
    return bytes(result)


print(base64_encode(bytes([0, 0, 0])))
print(base64_decode("aGVsbG8="))
