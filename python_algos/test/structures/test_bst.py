import itertools

import pytest

from python_algos.structures.bst import Node, BinarySearchTree


@pytest.fixture
def prefilled_node() -> Node[int]:
    r"""
    Result node structure
            0
        /       \
       -10       10
       / \      / \
     -15 -5    5  15
     /            /
    -15         15
    """
    root = Node(0)
    root.left = Node(-10)
    root.right = Node(10)
    root.left.left = Node(-15)
    root.left.right = Node(-5)
    root.right.left = Node(5)
    root.right.right = Node(15)
    root.left.left.left = Node(-15)
    root.right.right.left = Node(15)
    return root


def test_node_find(prefilled_node: Node[int]):
    assert prefilled_node.find(0) is prefilled_node
    assert prefilled_node.find(-5) is prefilled_node.left.right
    assert prefilled_node.find(-10) is prefilled_node.left
    assert prefilled_node.find(-15) is prefilled_node.left.left
    assert prefilled_node.find(5) is prefilled_node.right.left
    assert prefilled_node.find(10) is prefilled_node.right
    assert prefilled_node.find(15) is prefilled_node.right.right


def test_node_find_unknown(prefilled_node: Node[int]):
    # Test unknown values out of border of node
    assert prefilled_node.find(-10000) is None
    assert prefilled_node.find(10000) is None
    # Test unknown values in border of node (between 0 and +-5)
    assert prefilled_node.find(1) is None
    assert prefilled_node.find(-1) is None


def test_node_insert():
    root = Node(0)
    assert root.insert(-10) is root.left
    assert root.insert(-15) is root.left.left
    assert root.insert(-5) is root.left.right
    assert root.insert(10) is root.right
    assert root.insert(5) is root.right.left
    assert root.insert(15) is root.right.right
    # Test duplicates
    assert root.insert(-15) is root.left.left.left
    assert root.insert(15) is root.right.right.left


def test_node_traverse_inorder(prefilled_node: Node[int]):
    nodes_values = list(node.value for node in prefilled_node.traverse_inorder())
    assert nodes_values == [-15, -15, -10, -5, 0, 5, 10, 15, 15]


@pytest.fixture
def prefilled_tree() -> BinarySearchTree[int]:
    """
    Порядок аналогичен :see prefilled_node()
    """
    return _prefilled_tree()


def _prefilled_tree():
    tree = BinarySearchTree()
    tree.root = Node(0)
    tree.root.left = Node(-10)
    tree.root.right = Node(10)
    tree.root.left.left = Node(-15)
    tree.root.left.right = Node(-5)
    tree.root.right.left = Node(5)
    tree.root.right.right = Node(15)
    tree.root.left.left.left = Node(-15)
    tree.root.right.right.left = Node(15)
    return tree


def test_bst_find(prefilled_tree: BinarySearchTree[int]):
    assert prefilled_tree.find(0) is prefilled_tree.root
    assert prefilled_tree.find(-5) is prefilled_tree.root.left.right
    assert prefilled_tree.find(-10) is prefilled_tree.root.left
    assert prefilled_tree.find(-15) is prefilled_tree.root.left.left
    assert prefilled_tree.find(5) is prefilled_tree.root.right.left
    assert prefilled_tree.find(10) is prefilled_tree.root.right
    assert prefilled_tree.find(15) is prefilled_tree.root.right.right


def test_bst_find_unknown(prefilled_tree: BinarySearchTree[int]):
    # Test unknown values out of border of node
    assert prefilled_tree.find(-10000) is None
    assert prefilled_tree.find(10000) is None
    # Test unknown values in border of node (between 0 and +-5)
    assert prefilled_tree.find(1) is None
    assert prefilled_tree.find(-1) is None


def test_bst_find_empty():
    tree = BinarySearchTree()
    assert tree.find(0) is None


def test_bst_insert():
    tree = BinarySearchTree()
    assert tree.insert(0) is tree.root
    assert tree.insert(-10) is tree.root.left
    assert tree.insert(-15) is tree.root.left.left
    assert tree.insert(-5) is tree.root.left.right
    assert tree.insert(10) is tree.root.right
    assert tree.insert(5) is tree.root.right.left
    assert tree.insert(15) is tree.root.right.right
    # Test duplicates
    assert tree.insert(-15) is tree.root.left.left.left
    assert tree.insert(15) is tree.root.right.right.left


def test_bst_traverse_inorder(prefilled_tree: BinarySearchTree[int]):
    tree_values = list(node.value for node in prefilled_tree.traverse_inorder())
    assert tree_values == [-15, -15, -10, -5, 0, 5, 10, 15, 15]


def test_bst_remove_with_both_children(prefilled_tree: BinarySearchTree[int]):
    assert prefilled_tree.remove(0)
    assert prefilled_tree.root.value == 5
    assert [node.value for node in prefilled_tree.traverse_inorder()] == [
        -15,
        -15,
        -10,
        -5,
        5,
        10,
        15,
        15,
    ]


def test_bst_remove_with_left_child(prefilled_tree: BinarySearchTree[int]):
    assert prefilled_tree.remove(15)
    assert prefilled_tree.root.right.right.value == 15
    assert prefilled_tree.root.right.right.left is None
    assert [node.value for node in prefilled_tree.traverse_inorder()] == [
        -15,
        -15,
        -10,
        -5,
        0,
        5,
        10,
        15,
    ]


def test_bst_remove_with_right_child(prefilled_tree: BinarySearchTree[int]):
    # Insert 8 to right of 5 ( 5 -right-> 8 )
    prefilled_tree.insert(8)
    assert prefilled_tree.root.right.left.right.value == 8

    assert prefilled_tree.remove(5)
    assert prefilled_tree.root.right.left.value == 8
    assert prefilled_tree.root.right.left.right is None
    assert [node.value for node in prefilled_tree.traverse_inorder()] == [
        -15,
        -15,
        -10,
        -5,
        0,
        8,
        10,
        15,
        15,
    ]


def test_bst_remove_with_both_children_right_left_none(
    prefilled_tree: BinarySearchTree[int],
):
    # Insert -3 to right of -5 ( -5 -right-> -3 )
    prefilled_tree.insert(-3)
    assert prefilled_tree.root.left.right.right.value == -3

    assert prefilled_tree.remove(-5)
    assert prefilled_tree.root.left.right.value == -3
    assert [node.value for node in prefilled_tree.traverse_inorder()] == [
        -15,
        -15,
        -10,
        -3,
        0,
        5,
        10,
        15,
        15,
    ]


def test_bst_remove_unknown(prefilled_tree: BinarySearchTree[int]):
    assert not prefilled_tree.remove(-10000)
    assert not prefilled_tree.remove(10000)
    assert not prefilled_tree.remove(-1)
    assert not prefilled_tree.remove(1)


def test_bst_remove_empty():
    tree = BinarySearchTree()
    assert not tree.remove(0)


def test_bst_initial_values():
    initial_values = [-15, -15, -10, -5, 0, 5, 10, 15, 15]
    tree = BinarySearchTree(initial_values=initial_values)
    assert [node.value for node in tree.traverse_inorder()] == initial_values


@pytest.mark.skip(msg="Скипается из-за того, что выполняет 9! проверок")
def test_bst_remove_all_combinations():
    values = [-15, -15, -10, -5, 0, 5, 10, 15, 15]
    for permutation in itertools.permutations(values):
        tree = _prefilled_tree()
        for i, elem in enumerate(permutation):
            assert list(sorted(permutation[i:])) == [
                node.value for node in tree.traverse_inorder()
            ]
            tree.remove(elem)


def test_bst_iter(prefilled_tree: BinarySearchTree[int]):
    assert list(prefilled_tree) == [-15, -15, -10, -5, 0, 5, 10, 15, 15]


def test_bst_contains(prefilled_tree: BinarySearchTree[int]):
    for i in [-15, -15, -10, -5, 0, 5, 10, 15, 15]:
        assert i in prefilled_tree
    assert -100 not in prefilled_tree
    assert 100 not in prefilled_tree


def test_bst_len(prefilled_tree: BinarySearchTree[int]):
    assert len(prefilled_tree) == len([-15, -15, -10, -5, 0, 5, 10, 15, 15])


def test_bst_len_empty():
    tree = BinarySearchTree()
    assert len(tree) == 0
