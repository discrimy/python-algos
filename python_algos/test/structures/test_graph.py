from copy import deepcopy

import pytest

from python_algos.structures.graph import Graph


@pytest.fixture
def cyclic_graph() -> Graph:
    return Graph(
        {
            "A": {"B": 1, "C": 3},
            "B": {"A": 1, "C": 8},
            "C": {"A": 3, "B": 8},
        }
    )


@pytest.fixture
def acyclic_graph() -> Graph:
    return Graph(
        {
            "A": {"B": 3, "C": 4},
            "B": {"D": 2},
            "C": {"D": 4},
            "D": {"E": 4, "F": 3},
            "E": {"F": 7},
            "F": {},
        }
    )


adjustment_table = [
    [0, "A", "B", "C"],
    ["A", 0, 1, 3],
    ["B", 1, 0, 8],
    ["C", 3, 8, 0],
]


def test_adjustment_table(cyclic_graph: Graph):
    assert Graph.from_adjustment_table(adjustment_table) == cyclic_graph


def test_adjustment_table_incorrect():
    incorrect_adj_table = deepcopy(adjustment_table)
    incorrect_adj_table.pop()
    with pytest.raises(ValueError):
        Graph.from_adjustment_table(incorrect_adj_table)


def test_pairs(cyclic_graph: Graph):
    expected_pairs = [
        (("A", "B"), 1),
        (("B", "A"), 1),
        (("A", "C"), 3),
        (("C", "A"), 3),
        (("B", "C"), 8),
        (("C", "B"), 8),
    ]
    assert sorted(list(cyclic_graph.pairs())) == sorted(expected_pairs)


def test_pairs_of_empty():
    assert list(Graph().pairs()) == []


def test_routes(acyclic_graph: Graph):
    expected_routes = [
        ["A", "B", "D", "F"],
        ["A", "B", "D", "E", "F"],
        ["A", "C", "D", "F"],
        ["A", "C", "D", "E", "F"],
    ]
    assert sorted(list(acyclic_graph.routes("A", "F"))) == sorted(expected_routes)


def test_routes_cyclic(cyclic_graph: Graph):
    with pytest.raises(RecursionError):
        for _ in cyclic_graph.routes("A", "C"):
            pass


def test_filled_graph(acyclic_graph: Graph):
    filled_graph = acyclic_graph.as_filled_graph("A", "F")
    assert filled_graph == Graph(
        {
            "A": {"B": 2, "C": 4},
            "B": {"D": 2},
            "C": {"D": 4},
            "D": {"E": 4, "F": 2},
            "E": {"F": 4},
            "F": {},
        }
    )
    # Check source pressure
    assert sum(filled_graph["A"].values()) == 6
    # Check target pressure
    assert sum(weight for pair, weight in filled_graph.pairs() if pair[1] == "F") == 6
