import pytest
from hypothesis import given
from hypothesis.strategies import binary


from python_algos.other.base64_ import base64_decode, base64_encode


@given(binary(max_size=128))
def test_random(t: bytes):
    assert base64_decode(base64_encode(t)) == t


@pytest.mark.parametrize(
    ("decoded", "encoded"),
    [
        (b"", ""),
        (b"\x00\x00\x00", "AAAA"),
        (b"hello", "aGVsbG8="),
        (b"Hello world!", "SGVsbG8gd29ybGQh"),
    ],
)
def test_predefined(decoded: bytes, encoded: str):
    assert base64_encode(decoded) == encoded
    assert base64_decode(encoded) == decoded


@pytest.mark.parametrize(
    "malformed_encoded",
    [
        "AAA",  # len % 4 != 0
        "A===",  # 3+ =
        "AAA$",  # Unknown symbol
    ],
)
def test_malformed_data(malformed_encoded: str):
    with pytest.raises(ValueError):
        base64_decode(malformed_encoded)
