from typing import Callable, TypeVar, MutableSequence

T = TypeVar("T")


def generate_test_sort(sort_func: Callable[[MutableSequence[T]], None]):
    """
    Создаёт функцию для тестирования сортировки на месте через assert
    :param sort_func: функция, которую нужно тестировать
    :return: функция, которая сортирует original_seq и сверяет результат с expected_seq
    """

    def _test_sort(original_seq: MutableSequence[T], expected_seq: MutableSequence[T]):
        sort_func(original_seq)
        assert original_seq == expected_seq

    return _test_sort
