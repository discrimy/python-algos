from python_algos.sorting.bubble_sort import bubble_sort
from python_algos.test.sorting.utils import generate_test_sort

_test_sort = generate_test_sort(sort_func=bubble_sort)


def test_empty():
    _test_sort(
        original_seq=[],
        expected_seq=[],
    )


def test_one_element():
    _test_sort(
        original_seq=[1],
        expected_seq=[1],
    )


def test_sorted_seq():
    _test_sort(
        original_seq=[1, 2, 3, 4, 5, 6, 7, 8, 9],
        expected_seq=[1, 2, 3, 4, 5, 6, 7, 8, 9],
    )


def test_reversed_sorted_seq():
    _test_sort(
        original_seq=[9, 8, 7, 6, 5, 4, 3, 2, 1],
        expected_seq=[1, 2, 3, 4, 5, 6, 7, 8, 9],
    )


def test_shuffled_seq():
    _test_sort(
        original_seq=[1, 3, 7, 8, 2, 5, 9, 4, 6],
        expected_seq=[1, 2, 3, 4, 5, 6, 7, 8, 9],
    )


def test_duplicate():
    _test_sort(
        original_seq=[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
        expected_seq=[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
    )
