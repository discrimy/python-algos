from typing import (
    Generic,
    TypeVar,
    Optional,
    Generator,
    Iterable,
    Tuple,
    Sized,
    Container,
    Iterator,
)

T = TypeVar("T")


class Node(Sized, Generic[T]):
    """
    Узел двоичного дерева поиска (BST)

    Значения всех услов левого поддерева не больше значения корня.
    Значения всех услов правого поддерева больше значения корня.
    Все поддеревья - двоичные деревья поиска.

    Тип значения должен быть сравниваемым (поддерживать операции ==, >, <)
    """

    value: T
    left: Optional["Node[T]"]
    right: Optional["Node[T]"]

    def __init__(self, value: T):
        self.value = value
        self.left = None
        self.right = None

    def find_with_parent(
        self, searching_value: T, self_parent: Optional["Node[T]"]
    ) -> Optional[Tuple["Node[T]", Optional["Node[T]"]]]:
        """
        Ищет элемент в дереве и возвращает его вместе с родителем
        :param searching_value: искомое значение
        :param self_parent: родитель текущего узла
        :return: (<узел>, <родитель или None>) или None
        """
        if searching_value == self.value:
            return self, self_parent
        if searching_value < self.value and self.left is not None:
            return self.left.find_with_parent(searching_value, self)
        if searching_value > self.value and self.right is not None:
            return self.right.find_with_parent(searching_value, self)
        return None

    def find(self, searching_value: T) -> Optional["Node[T]"]:
        """
        Ищет значение в себе и своих поддеревьях (если есть)

        :param searching_value: искомое значение
        :return: узел, у которого такое же значение или None
        """
        result = self.find_with_parent(searching_value, self)
        if result is None:
            return None
        return result[0]

    def insert(self, inserting_value: T) -> "Node[T]":
        """
        Вставляет значение в свои поддеревья

        :param inserting_value: значение, которое надо вставить
        :return: узел, который был вставлен в дерево
        """
        if inserting_value <= self.value:
            if self.left is None:
                self.left = Node(inserting_value)
                return self.left
            else:
                return self.left.insert(inserting_value)
        if inserting_value > self.value:
            if self.right is None:
                self.right = Node(inserting_value)
                return self.right
            else:
                return self.right.insert(inserting_value)

    def traverse_inorder(self) -> Generator["Node[T]", None, None]:
        """
        Генератор, проходящийся по всем узлам дерева по неубыванию

        :return: узлы дерева по неубыванию
        """
        if self.left is not None:
            yield from self.left.traverse_inorder()
        yield self
        if self.right is not None:
            yield from self.right.traverse_inorder()

    def __str__(self):
        return f"Node({self.value})"

    def __len__(self) -> int:
        return (
            1
            + (len(self.left) if self.left is not None else 0)
            + (len(self.right) if self.right is not None else 0)
        )


class BinarySearchTree(Iterable[T], Container[T], Sized, Generic[T]):
    """
    Двоичное дерево поиска
    """

    root: Optional[Node[T]]

    def __init__(self, initial_values: Optional[Iterable[T]] = None):
        """
        :param initial_values: изначальные значения (порядок не важен).
        Если None, то дерево будет пустым
        """
        self.root = None
        if initial_values is not None:
            for elem in initial_values:
                self.insert(elem)

    def find(self, elem: T) -> Optional[Node[T]]:
        """
        Ищет элемент в дереве

        :param elem: искомый элемент
        :return: узел с таким значением или None
        """
        if self.root is None:
            return None
        return self.root.find(elem)

    def insert(self, elem: T) -> Node[T]:
        """
        Вставляет элемент в дерево

        :param elem: вставляемый элемент
        :return: узел, в который вставлен элемент
        """
        if self.root is None:
            self.root = Node(elem)
            return self.root
        return self.root.insert(elem)

    def traverse_inorder(self) -> Generator[Node[T], None, None]:
        """
        Возвращает узлы дерева в порядке неубывания

        :return: узлы дерева в порядке неубывания
        """
        if self.root is not None:
            yield from self.root.traverse_inorder()

    def remove(self, elem: T) -> bool:
        """
        Удаляет из дерева узел с таким значением
        :param elem: удаляемое значение
        :return: False если такого узла нет, True если был и удалён
        """
        if self.root is None:
            return False

        find_result = self.root.find_with_parent(elem, None)
        if find_result is None:
            return False
        node, parent_node = find_result

        # Случай 1: у узла нет правого потомка
        if node.right is None:
            if parent_node is None:
                self.root = node.left
            elif node is parent_node.left:
                parent_node.left = node.left
            else:
                parent_node.right = node.left
            return True

        # Случай 2: у узла есть правый потомок, но у него нет левого потомка
        if node.right is not None and node.right.left is None:
            node.right.left = node.left
            if parent_node is None:
                self.root = node.right
            elif node is parent_node.left:
                parent_node.left = node.right
            else:
                parent_node.right = node.right
            return True

        # Случай 3: у правого потомка есть левый подпотомок
        if node.right is not None and node.right.left is not None:
            # Находим минимальный узель у правого потомка с его родителем
            min_child_node_parent = node.right
            min_child_node = node.right.left
            while min_child_node.left is not None:
                min_child_node_parent = min_child_node
                min_child_node = min_child_node.left

            # Заменяем мин. узел на его правое поддерево (левое у него будет пустым)
            min_child_node_parent.left = min_child_node.right
            # Заменяем у мин. узла его потомков на потомков удаляемого
            min_child_node.left = node.left
            min_child_node.right = node.right
            # Заменяем удаляемый узел на минимальный
            if parent_node is None:
                self.root = min_child_node
            elif node is parent_node.left:
                parent_node.left = min_child_node
            else:
                parent_node.right = min_child_node
            return True

    def __iter__(self) -> Iterator[T]:
        for node in self.traverse_inorder():
            yield node.value

    def __contains__(self, elem: T) -> bool:
        return self.find(elem) is not None

    def __len__(self) -> int:
        if self.root is None:
            return 0
        return len(self.root)
