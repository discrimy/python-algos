from collections import UserDict
from copy import deepcopy
from typing import List, Generator, Tuple

Node = str
Weight = int


class Graph(UserDict):
    """
    Weighted oriented graph class, described as {Node: {NeighbourNode: Weight}}
    """

    @classmethod
    def from_adjustment_table(cls, table):
        """
        Creates Graph instance from adjustment table
        Example:
          A B C  -> {
        A 0 1 3      'A': {'B': 1, 'C': 3},
        B 1 0 8      'B': {'A': 1, 'C': 8},
        C 1 8 0      'C': {'A': 3, 'B': 8}
                    }
        :param table: adjustment table
        :return: a graph instance
        :raise ValueError: a table has an incorrect size (not a square table)
        """
        table_size = len(table)
        if not all(table_size == len(row) for row in table):
            raise ValueError(f"Table must be a square")

        graph = cls()
        for i1, elem1 in enumerate(table[0][1:], 1):
            for i2, elem2 in enumerate((table[i][0] for i in range(1, len(table))), 1):
                if elem1 == elem2:
                    continue
                if elem1 not in graph:
                    graph[elem1] = {}
                graph[elem1][elem2] = table[i1][i2]
        return graph

    def pairs(self) -> Generator[Tuple[Tuple[Node, Node], Weight], None, None]:
        """
        Returns a generator of all pairs in graph with their weight. An order is unspecified
        (example (('A', 'B'), 3) )
        :return:
        """
        for node in self.data:
            for neighbour, weight in self.data[node].items():
                yield (node, neighbour), weight

    def routes(
        self, from_node: Node, to_node: Node
    ) -> Generator[List[Node], None, None]:
        """
        Returns a generator of all possible routes from first node to second one.
        If a graph has a cycle, it will never end (just get new routes with n numbers of cycled part)
        :param from_node: first node of route
        :param to_node: last node of route
        :return: routes (format [from_node, ..., to_node])
        :raise RecursionError: graph has a cycle
        """
        if from_node == to_node:
            yield [from_node]

        for neighbour in self.data[from_node].keys():
            for route in self.routes(neighbour, to_node):
                yield [from_node] + route

    def as_filled_graph(self, from_node: Node, to_node: Node) -> "Graph":
        """
        Returns a graph with same structure, but weight will be a max 'pressure' (or anything else)
        that an initial graph can handle per a pair of nodes.
        Max pressure can be obtained as sum(graph[root_node].values())
        :param from_node: root node, a source of pressure
        :param to_node: end node, a sink of pressure
        :return: Weighted graph
        """
        graph_with_flows = deepcopy(self)
        for node in graph_with_flows.keys():
            for neighbour in graph_with_flows[node]:
                graph_with_flows[node][neighbour] = 0

        for route in self.routes(from_node, to_node):
            flows = []
            for node, next_node in (
                (route[i], route[i + 1]) for i in range(len(route) - 1)
            ):
                flows.append(
                    self.data[node][next_node] - graph_with_flows[node][next_node]
                )
            min_flow = min(flows)
            for node, next_node in (
                (route[i], route[i + 1]) for i in range(len(route) - 1)
            ):
                graph_with_flows[node][next_node] += min_flow
        return graph_with_flows
