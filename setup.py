from setuptools import setup, find_packages

setup(
    name="python-algos",
    version="0.1",
    description="Python algorithms",
    author="Alexander Bespalov",
    author_email="discrimy@gmail.com",
    license="MIT",
    packages=find_packages(),
    zip_safe=False,
)
